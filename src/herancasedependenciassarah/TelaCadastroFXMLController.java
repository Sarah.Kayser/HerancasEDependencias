package herancasedependenciassarah;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Sarah Kayser <sarah.bederode@gmail.com>
 */
public class TelaCadastroFXMLController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private Button cadastrarButton;

    @FXML
    private TableView<Pessoa> tabelaCadastro;
    @FXML
    private TableColumn<Pessoa, String> nomeColumn;
    @FXML
    private TableColumn<Pessoa, String> codigoColumn;
    @FXML
    private TableColumn<Pessoa, Integer> idadeColumn;
    @FXML
    private TableColumn<Pessoa, String> enderecoColumn;
    @FXML
    private TableColumn<Pessoa, String> funcaoColumn;
    @FXML
    private TableColumn<Pessoa, Integer> semestreColumn;
    @FXML
    private TableColumn<Pessoa, String> cursoColumn;
    @FXML
    private TableColumn<Pessoa, Integer> salarioColumn;
    @FXML
    private TableColumn<Pessoa, String> disciplinaColumn;
    @FXML
    private TableColumn<Pessoa, String> setorColumn;
    
    @FXML
    private RadioButton alunoRadioButton;
    @FXML
    private RadioButton professorRadioButton;
    @FXML
    private RadioButton funcadmRadioButton;
    
    @FXML
    private TextField nomeText;
    @FXML
    private TextField idadeText;
    @FXML
    private TextField enderecoText;
    @FXML
    private TextField codigoText;
    
    @FXML
    private TextField salarioText;
    @FXML
    private TextField setorText;
    @FXML
    private TextField funcaoText;
    
    @FXML
    private TextField semestreText;
    @FXML
    private TextField cursoText;
    
    @FXML
    private TextField disciplinaText;
    
    private final ArrayList<FuncAdm> func = new ArrayList();
    private final ArrayList<Professor> prof = new ArrayList();
    private final ArrayList<Aluno> al = new ArrayList();
    
    private int numero;

    @FXML
    void clickCadastrar(ActionEvent event) {
        if (1==numero){
            FuncAdm u = new FuncAdm();
            u.setCodigo(codigoText.getText());
            u.setNome(nomeText.getText());
            u.setIdade(Integer.parseInt(idadeText.getText()));
            u.setEndereco(enderecoText.getText());
            u.setSalario(Double.parseDouble(salarioText.getText()));
            func.add(u);
            //mensagemSistema.setText("Cadastro feito!");
        } else if (2==numero){
            Professor p = new Professor();
            p.setCodigo(codigoText.getText());
            p.setNome(nomeText.getText());
            p.setIdade(Integer.parseInt(idadeText.getText()));
            p.setEndereco(enderecoText.getText());
            prof.add(p);
            //mensagemSistema.setText("Cadastro feito!");
        } else{
            Aluno a = new Aluno();
            a.setCodigo(codigoText.getText());
            a.setNome(nomeText.getText());
            a.setIdade(Integer.parseInt(idadeText.getText()));
            a.setEndereco(enderecoText.getText());
            al.add(a);
        }
    }
    
    
    
    @FXML
    void escolherFuncionario (MouseEvent event){
        salarioText.setVisible(true);
        funcaoText.setVisible(true);
        setorText.setVisible(true);
        semestreText.setVisible(false);
        cursoText.setVisible(false);
        disciplinaText.setVisible(false);
        numero=1;
    }
    
    @FXML
    void escolherProfessor (MouseEvent event){
        salarioText.setVisible(true);
        funcaoText.setVisible(false);
        setorText.setVisible(false);
        semestreText.setVisible(false);
        cursoText.setVisible(false);
        disciplinaText.setVisible(true);
        numero=2;
    }
    
    @FXML
    void escolherAluno (MouseEvent event){
        salarioText.setVisible(false);
        funcaoText.setVisible(false);
        setorText.setVisible(false);
        semestreText.setVisible(true);
        cursoText.setVisible(true);
        disciplinaText.setVisible(false);
        numero=3;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
}
