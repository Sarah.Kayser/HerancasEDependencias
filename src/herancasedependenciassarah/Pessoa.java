package herancasedependenciassarah;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Sarah Kayser <sarah.bederode@gmail.com>
 */

public abstract class Pessoa {
    private String codigo;
    private String nome;
    private int idade;
    private String endereco;
    
    public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
    
        String  insertTableSQL = "INSERT INTO OO_ExercicioPessoa (codigo,nome,idade,endereco) VALUES (?,?,?,?)";
        
        try {
            PreparedStatement prepareStatement = dbConnection.prepareStatement(insertTableSQL);
            
            prepareStatement.setString(1,getCodigo());
            prepareStatement.setString(2,getNome());
            prepareStatement.setInt(3,getIdade());
            prepareStatement.setString(4,getEndereco());
       
            prepareStatement.executeUpdate();
       
        }catch(SQLException e){
        }
    }
    
    public String getCodigo() {
        return codigo;
    }
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public int getIdade() {
        return idade;
    }
    public void setIdade(int idade) {
        this.idade = idade;
    }
    
    public String getEndereco() {
        return endereco;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
}
