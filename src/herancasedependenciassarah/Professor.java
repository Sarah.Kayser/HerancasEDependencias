package herancasedependenciassarah;

/**
 *
 * @author Sarah Kayser <sarah.bederode@gmail.com>
 */
public class Professor extends Funcionario {
    private String disciplina;

    public String getDisciplina() {
        return disciplina;
    }
    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }
}
