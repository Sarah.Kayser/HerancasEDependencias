package herancasedependenciassarah;

/**
 *
 * @author Sarah Kayser <sarah.bederode@gmail.com>
 */
public abstract class Funcionario extends Pessoa {
    private double salario;

    public double getSalario() {
        return salario;
    }
    public void setSalario(double salario) {
        this.salario = salario;
    }
}
